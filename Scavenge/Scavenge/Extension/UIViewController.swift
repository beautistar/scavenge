//
//  UIViewControllerExtension.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 1/19/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func setNavigationBarItem() {
        //self.addRightBarButtonWithImage(UIImage(named: "menu")!)
        self.addLeftBarButtonWithImage(UIImage(named: "menu")!)
        self.navigationController?.navigationBar.barTintColor = UIColor(hex: "F5FCFF")
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.addLeftGestures()
    }
    
    func removeNavigationBarItem() {
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = nil
        self.slideMenuController()?.removeLeftGestures()
    }
    
    func setNavigationBarTitle() {
        if SelectedGame?.photo_url == nil {}else {
            
            let navView = UIView()
            let label = UILabel()
            label.text = SelectedGame?.name
            label.sizeToFit()
            label.center = navView.center
            label.textAlignment = NSTextAlignment.center
            let image = UIImageView()
            image.kf.setImage(with: URL(string: SelectedGame!.photo_url))
            let imageAspect = image.image!.size.width / image.image!.size.height
            image.frame = CGRect(x: label.frame.origin.x-label.frame.size.height*imageAspect-5, y: label.frame.origin.y, width: label.frame.size.height*imageAspect, height: label.frame.size.height)
            image.contentMode = UIView.ContentMode.scaleAspectFit
            navView.addSubview(label)
            navView.addSubview(image)
            self.navigationItem.titleView = navView
            navView.sizeToFit()
        }
    }
    
    func setNavigationBarRightButton() {
        
        let button: UIButton = UIButton(type: UIButton.ButtonType.custom)
        let tap = UITapGestureRecognizer(target: self, action: #selector(onTapRightBtn))
        button.addGestureRecognizer(tap)
        button.setImage(UIImage(named: "profile_logo"), for: .normal)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func onTapRightBtn() {
        
       if let url = URL(string: "http://scavengeski.com") {
           UIApplication.shared.open(url)
       }
    }
}
