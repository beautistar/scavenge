//
//  ImageVC.swift
//  Scavenge
//
//  Created by Developer on 3/1/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class ImageVC: UIViewController {
    
    @IBOutlet weak var imvPicture: UIImageView!
    var urlString = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        imvPicture.kf.indicatorType = .activity
        imvPicture.kf.setImage(with: URL(string: urlString))
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
