//
//  MainVC.swift
//  Scavenge
//
//  Created by Developer on 2/19/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit
import CarbonKit
import Kingfisher

class MainVC: BaseViewController, CarbonTabSwipeNavigationDelegate {
    
    @IBOutlet weak var contentView: UIView!    
    var tabSwipe: CarbonTabSwipeNavigation!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        from = .main
        setNavigationBarItem()
        setNavigationBarRightButton()
       
        tabSwipe = CarbonTabSwipeNavigation(items: ["Activity feed", "Scoreboard"], delegate: self)
        // Custimize segmented control
        tabSwipe.carbonSegmentedControl?.setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Raleway-SemiBold", size: 16)!, NSAttributedString.Key.foregroundColor: UIColor.gray], for: .normal)
        tabSwipe.carbonSegmentedControl?.setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Raleway-SemiBold", size: 16)!, NSAttributedString.Key.foregroundColor: Const.COLOR_MAIN!], for: .selected)
        tabSwipe.insert(intoRootViewController: self, andTargetView: contentView)
        
        style()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNavigationBarTitle()
    }
    
    func style() {       

        let widthOfTabIcons = ScreenRect.width/2
        tabSwipe.carbonSegmentedControl!.setWidth(widthOfTabIcons, forSegmentAt: 0)
        tabSwipe.carbonSegmentedControl!.setWidth(widthOfTabIcons, forSegmentAt: 1)
        tabSwipe.toolbarHeight.constant = 50
        tabSwipe.toolbar.isTranslucent = false
        tabSwipe.toolbar.barTintColor = Const.COLOR_BG
        tabSwipe.setIndicatorColor(Const.COLOR_MAIN!)        

    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        guard let storyboard = storyboard else { return UIViewController() }
        if index == 0 {
            return storyboard.instantiateViewController(withIdentifier: "ActivityFeedVC")
        }
        return storyboard.instantiateViewController(withIdentifier: "ScoreboardVC")
    }

}

extension MainVC : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}

