//
//  MyActivity.swift
//  Scavenge
//
//  Created by Developer on 2/20/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class MyActivityVC: BaseViewController {
    
    var isRemainTask: Bool = true

    @IBOutlet weak var btnRemainTask: UIButton!
    @IBOutlet weak var btnCompletedTask: UIButton!
    @IBOutlet weak var cvCategoryList: UICollectionView!
    @IBOutlet weak var tblTask: UITableView!
    
    var categoryList = [CategoryModel]()
    var remainTaskList = [TaskModel]()
    var completedTaskList = [TaskModel]()
    var selectedCategories = [String]()
    var filteredTaskList = [TaskModel]()
    var totalRemainTaskList = [TaskModel]()
    var totalCompletedTaskList = [TaskModel]()

    override func viewDidLoad() {
        super.viewDidLoad()

        categoryList.removeAll()
        
        ApiRequest.get_category { (categories, message) in
            self.categoryList = categories
            self.cvCategoryList.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
                
        if from == .task {
            self.setNavigationBarItem()
            self.view.backgroundColor = Const.COLOR_BG
            self.title = "Task"
            
        } else {
            self.view.backgroundColor = .clear            
        }
        
        if SelectedGame == nil {
            self.showAlert(title: Const.APPNAME, message: "Please select a game", okButtonTitle: Const.OK, cancelButtonTitle: nil) {
                self.gotoGame()
            }
        } else {
            getTask(SelectedGame!)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        selectedCategories.removeAll()
        for i in 1...categoryList.count-1 {
            categoryList[i].selectState = false
        }
    }
    
    func getTask(_ game: GameModel) {
        self.showLoadingView()
        ApiRequest.get_task(game.id) { (tasks, message) in
            self.hideLoadingView()
            self.remainTaskList = tasks.filter {($0.is_completed == "no")}
            self.completedTaskList = tasks.filter {($0.is_completed == "yes")}
            self.totalRemainTaskList = tasks.filter {($0.is_completed == "no")}
            self.totalCompletedTaskList = tasks.filter {($0.is_completed == "yes")}
            self.tblTask.reloadData()
        }
    }
    
    func gotoGame() {
        
        popCount = 3
        let gameVC = self.storyboard?.instantiateViewController(withIdentifier: "GameVC") as! GameVC        
        self.navigationController?.pushViewController(gameVC, animated: true)
    }

    @IBAction func onSwitchTask(_ sender: Any) {
        
        let btnSwitch: UIButton = sender as! UIButton
        
        if btnSwitch.tag == 0 {
            selectedCategories.removeAll()
            isRemainTask = true
            
            for item in categoryList {
                item.selectState = false
            }
        } else {
            selectedCategories.removeAll()
            isRemainTask = false
            
            for item in categoryList {
                item.selectState = false
            }
        }
        
        changeTaskView()
        tblTask.reloadData()
        cvCategoryList.reloadData()
    }
    
    func changeTaskView() {
    
        if isRemainTask {
            
            btnRemainTask.setTitleColor(.white, for: .normal)
            btnRemainTask.backgroundColor = Const.COLOR_MAIN
            btnCompletedTask.setTitleColor(.black, for: .normal)
            btnCompletedTask.backgroundColor = .white
                        
        }else{
           
            btnRemainTask.setTitleColor(.black, for: .normal)
            btnRemainTask.backgroundColor = .white
            btnCompletedTask.setTitleColor(.white, for: .normal)
            btnCompletedTask.backgroundColor = Const.COLOR_MAIN
        }
    }
    
    @objc func onPlus(_ sender: UIButton) {           
           
        let addPostVC = self.storyboard?.instantiateViewController(withIdentifier: "AddTaskVC") as! AddTaskVC
        addPostVC.task = remainTaskList[sender.tag]
        self.navigationController?.pushViewController(addPostVC, animated: true)
    }
    
    func getTaskByFilter() {
        
        print("total remain", totalRemainTaskList.count)
        remainTaskList.removeAll()
        //self.showLoadingView()
        //for item in selectedCategories {

        self.remainTaskList = totalRemainTaskList.filter {(selectedCategories.contains($0.category))}
        self.completedTaskList = completedTaskList.filter {(selectedCategories.contains($0.category))}
        //}
        //hideLoadingView()
        print("filtered remain", remainTaskList.count)
        print("remainTaskList====", remainTaskList)
        print("completedTaskList=====", completedTaskList)
        self.tblTask.reloadData()        
    }
}

extension MyActivityVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isRemainTask {
            return remainTaskList.count
        } else {
            return completedTaskList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskCell") as! TaskCell
    
        if isRemainTask {
            cell.btnPlus.isHidden = false
            cell.btnPlus.tag = indexPath.row
            cell.btnPlus.addTarget(self, action: #selector(onPlus(_:)), for: .touchUpInside)
            cell.entity = remainTaskList[indexPath.row]
        } else {
            cell.entity = completedTaskList[indexPath.row]
            cell.btnPlus.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        return (ScreenRect.width - 30) / 2.0 / 1.3 + 20
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension MyActivityVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return categoryList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
        cell.entity = categoryList[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let w = CGFloat(categoryList[indexPath.row].name.count * 6 + 20)
        return CGSize(width: w, height: 30)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        categoryList[indexPath.row].selectState = !categoryList[indexPath.row].selectState
        
        if indexPath.row == 0 {
            for i in 1...categoryList.count-1 {
                
                categoryList[i].selectState = categoryList[0].selectState
                if categoryList[0].selectState == true {
                    selectedCategories.append(categoryList[i].name)
                }else {
                    selectedCategories.removeAll()
                }
            }
            self.getTaskByFilter()
        } else {
            categoryList[0].selectState = false
            if categoryList[indexPath.row].selectState == false {
                
                print(categoryList[indexPath.row].name)
                if let index = selectedCategories.firstIndex(of: categoryList[indexPath.row].name) {
                    print(index)
                    self.selectedCategories.remove(at: index)
                }
            } else {
                self.selectedCategories.append(categoryList[indexPath.row].name)
            }
            let selectedCount = categoryList.filter{ ($0.selectState == true) }.count
           
            if selectedCount >= categoryList.count - 1 && categoryList[0].selectState == false {
                categoryList[0].selectState = true
            }
            
            self.getTaskByFilter()
        }
        
        cvCategoryList.reloadData()
    }
}


