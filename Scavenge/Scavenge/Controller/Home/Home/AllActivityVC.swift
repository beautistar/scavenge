//
//  AllActivityVC.swift
//  Scavenge
//
//  Created by Developer on 2/20/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class AllActivityVC: BaseViewController {
    
    @IBOutlet weak var tblPost: UITableView!
    
    var postList = [PostModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if SelectedGame == nil {
            self.showAlert(title: Const.APPNAME, message: "Please select a game", okButtonTitle: Const.OK, cancelButtonTitle: nil) {
                self.gotoGame()
            }
        } else {
            getAllPost()
        }
    }
    
    func getAllPost() {
        
        ApiRequest.get_all_post(SelectedGame!.id) { (posts, message) in
            
            self.postList = posts
            self.tblPost.reloadData()
        }
    }
    
    func gotoGame() {
        
        popCount = 3
        let gameVC = self.storyboard?.instantiateViewController(withIdentifier: "GameVC") as! GameVC
        self.navigationController?.pushViewController(gameVC, animated: true)
    }
    
    @objc func onPlayVideo(_ sender: UIButton) {
        
        if let url = URL(string: postList[sender.tag].video_url) {
            let player = AVPlayer(url: url)
            let vc = AVPlayerViewController()
            vc.player = player
            self.present(vc, animated: true) { vc.player?.play() }
        } else {
            self.showAlert("Not able to play video")
        }
    }
}


extension AllActivityVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return postList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell") as! PostCell
        cell.entity = postList[indexPath.row]
        cell.btnPlayVideo.tag = indexPath.row
        cell.btnPlayVideo.addTarget(self, action: #selector(onPlayVideo), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return (ScreenRect.width - 30) / 2.0 / 1.3 + 20
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let entity = postList[indexPath.row]
        if entity.video_url == "" {
            let imageVC = self.storyboard?.instantiateViewController(withIdentifier: "ImageVC") as! ImageVC
            imageVC.urlString = entity.photo_url
            imageVC.modalPresentationStyle = .fullScreen
            self.present(imageVC, animated: true, completion: nil)
        }
    }
}
