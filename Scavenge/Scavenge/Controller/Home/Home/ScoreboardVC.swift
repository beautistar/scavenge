//
//  ScoreboardVC.swift
//  Scavenge
//
//  Created by Developer on 2/19/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit
import ExpyTableView

class ScoreboardVC: BaseViewController {

    @IBOutlet weak var imvTeam: UIImageView!
    @IBOutlet weak var imvIndividual: UIImageView!
    @IBOutlet weak var tblScoreboard: ExpyTableView!
    
    var teamSectionData = [TeamModel]()
    var individualData = [IndividualModel]()
    var isTeamMode = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tblScoreboard.tableFooterView = UIView()
        
        if from == .task {
            self.setNavigationBarItem()
            self.view.backgroundColor = Const.COLOR_BG
            
        } else {
            self.view.backgroundColor = .clear
        }        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if SelectedGame != nil {
            getTeam()
        } else {
            self.showAlert(title: Const.APPNAME, message: "Please select a game", okButtonTitle: Const.OK, cancelButtonTitle: nil) {
                self.gotoGame()
            }
        }
    }
    
    func gotoGame() {
        popCount = 3
        let gameVC = self.storyboard?.instantiateViewController(withIdentifier: "GameVC") as! GameVC
        self.navigationController?.pushViewController(gameVC, animated: true)
    }
    
    func getTeam() {
        
        self.showLoadingView()
        
        ApiRequest.get_team(SelectedGame!.id) { (teams, individuals, message) in
            self.hideLoadingView()
            if message == Const.MSG_SUCCESS {
                self.teamSectionData = teams
                self.individualData = individuals.sorted(by: {$0.score > $1.score })
                print(individuals.count)
                self.tblScoreboard.reloadData()
            } else {
                self.showAlert(message)
            }
        }
    }
    

    @IBAction func onSwitch(_ sender: UIButton) {
       
        if sender.tag == 0 {
            isTeamMode = true
            imvTeam.image = UIImage(named: "team_btn_active")
            imvIndividual.image = UIImage(named: "individual_btn_inactive")
            imvTeam.backgroundColor = UIColor(named: "MainBlueColor")
            imvIndividual.backgroundColor = UIColor.white
            
        } else {
            isTeamMode = false
            imvTeam.image = UIImage(named: "team_btn_inactive")
            imvIndividual.image = UIImage(named: "individual_btn_active")
            imvTeam.backgroundColor = UIColor.white
            imvIndividual.backgroundColor = UIColor(named: "MainBlueColor")
            
        }
        
        tblScoreboard.reloadData()
    }
}


extension ScoreboardVC: ExpyTableViewDataSource {
    func tableView(_ tableView: ExpyTableView, expandableCellForSection section: Int) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HeaderTableViewCell.self)) as! HeaderTableViewCell
        //Make your customizations here.
        //cell.labelHeader.text = "Section: \(section) Row: 0"
        
        if isTeamMode {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TeamCell") as! TeamCell
       
            cell.backgroundColor = .clear
           
            cell.entity = teamSectionData[section]
            
            return cell
           
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "IndividualCell") as! IndividualCell
            if section % 2 == 0 {
                cell.backgroundColor = .white
            } else {
                cell.backgroundColor = .clear
                
            }
            cell.entity = individualData[section]
            cell.imvPhotoXConstraint.constant = 0
            cell.lblTeamNameXConstraint.constant = 20
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        print("section", section)
        return 0
    }
}

//MARK: Basic Table View Implementation, no need to write UITableViewDataSource because ExpyTableViewDataSource is forwarding all the delegate methods of UITableView that are not handled by itself.

extension ScoreboardVC {
    func numberOfSections(in tableView: UITableView) -> Int {
        if isTeamMode {
            return teamSectionData.count
        } else {
            return individualData.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Please see https://github.com/okhanokbay/ExpyTableView/issues/12
        // The cell instance that you return from expandableCellForSection: data source method is actually the first row of belonged section. Thus, when you return 4 from numberOfRowsInSection data source method, first row refers to expandable cell and the other 3 rows refer to other rows in this section.
        // So, always return the total row count you want to see in that section
        if isTeamMode {
            return teamSectionData[section].members.count + 1
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print("cellForRowAt", indexPath.row)
        
        // If you define a cell as expandable and return it from expandingCell data source method,
        // then you will not get callback for IndexPath(row: 0, section: indexPath.section) here in cellForRowAtIndexPath
        //But if you define the same cell as -sometimes not expandable- you will get callbacks for not expandable cells here and you must return a cell for IndexPath(row: 0, section: indexPath.section) in here besides in expandingCell. You can return the same cell from expandingCell method and here.
            
        if !isTeamMode {

            return UITableViewCell()

        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "IndividualCell") as! IndividualCell
            
            cell.backgroundColor = .white

            cell.entity = teamSectionData[indexPath.section].members[indexPath.row-1]
            cell.imvPhotoXConstraint.constant = 20
            cell.lblTeamNameXConstraint.constant = 35
            return cell
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if isTeamMode {
            if indexPath.row == 0 {
                return 60
                
            }
            else {
                return 100
            }
        } else {
            return 100
        }
    }
}

extension ScoreboardVC: ExpyTableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //If you don't deselect the row here, seperator of the above cell of the selected cell disappears.
        //Check here for detail: https://stackoverflow.com/questions/18924589/uitableviewcell-separator-disappearing-in-ios7
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        //This solution obviously has side effects, you can implement your own solution from the given link.
        //This is not a bug of ExpyTableView hence, I think, you should solve it with the proper way for your implementation.
        //If you have a generic solution for this, please submit a pull request or open an issue.
        
        print("DID SELECT row: \(indexPath.row), section: \(indexPath.section)")
    }
    
//    func tableView(_ tableView: ExpyTableView, canExpandSection section: Int) -> Bool {
//
//        if isTeamMode {
//            return true //Return false if you want your section not to be expandable
//        }
//
//        return false
//
//    }
}




