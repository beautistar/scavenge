//
//  ActivityFeedVC.swift
//  Scavenge
//
//  Created by Developer on 2/19/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

var popCount = 0
class ActivityFeedVC: BaseViewController {

    @IBOutlet weak var btnAllActivity: UIButton!
    @IBOutlet weak var btnMyActivity: UIButton!
    var container: ContainerViewController!
    
    var isAllActivity: Bool = false
    
    var delegate: LeftMenuProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
        
        if from == .task {
            self.setNavigationBarItem()
            self.view.backgroundColor = Const.COLOR_BG
            self.title = "Task"
            isAllActivity = false
            
        } else {
            self.title = "Activity Feed"
            self.view.backgroundColor = .clear
            isAllActivity = true
            
        }
        changeActivity()
        
        if SelectedGame == nil {
            self.showAlert(title: Const.APPNAME, message: "Please select a game", okButtonTitle: Const.OK, cancelButtonTitle: nil) {
                self.gotoGame()
            }
        }
        
    }
        
    func gotoGame() {
        popCount = 3
        let gameVC = self.storyboard?.instantiateViewController(withIdentifier: "GameVC") as! GameVC
        self.navigationController?.pushViewController(gameVC, animated: true)
    }

    @IBAction func onChangeActivity(_ sender: UIButton) {
        
        if sender.tag == 0{
            isAllActivity = true
        }else{
            isAllActivity = false
            //let controller = container.currentViewController as? AllActivityVC
            //controller?.delegate = delegate
        }
        
        changeActivity()
    }
    
    func changeActivity() {
        
        if isAllActivity {
            
            btnAllActivity.setTitleColor(.white, for: .normal)
            btnAllActivity.backgroundColor = Const.COLOR_MAIN
            btnMyActivity.setTitleColor(.black, for: .normal)
            btnMyActivity.backgroundColor = .white
            container!.segueIdentifierReceivedFromParent("SegueAllActivity")
            
        }else{
            btnAllActivity.setTitleColor(.black, for: .normal)
            btnAllActivity.backgroundColor = .white
            btnMyActivity.setTitleColor(.white, for: .normal)
            btnMyActivity.backgroundColor = Const.COLOR_MAIN
            container!.segueIdentifierReceivedFromParent("SegueMyActivity")
            //let controller = container.currentViewController as? MyActivityVC
            //controller?.delegate = self            
        }
    }
    
    /*
     
     
    @IBAction func getText(_ sender: UIButton) {
        
        
        if let getFirstVCObject = self.container.currentViewController as? FirstViewController{
            let getText = getFirstVCObject.firstVCTextfield.text!
            print(getText)
        }
        
    }
    
    
    
    @IBAction func sendAction(_ sender: AnyObject) {
        
        if container.currentViewController.isKind(of: FirstViewController.self){
            
            if let getFirstVCObject = self.container.currentViewController as? FirstViewController
            {
                let getText = self.sendTextField.text
                getFirstVCObject.firstVCLabel.text  = getText!
            }
        }
        else if container.currentViewController.isKind(of: SecondViewController.self){
            
            if let getSecondVCObject = self.container.currentViewController as? SecondViewController
            {
                let getText = self.sendTextField.text
                getSecondVCObject.secondVCLabel.text = getText!
                
            }
        }
    }
 
    */
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "container"{
            container = segue.destination as? ContainerViewController
            //For adding animation to the transition of containerviews you can use container's object property
            // animationDurationWithOptions and pass in the time duration and transition animation option as a tuple
            // Animations that can be used
            // .transitionFlipFromLeft, .transitionFlipFromRight, .transitionCurlUp
            // .transitionCurlDown, .transitionCrossDissolve, .transitionFlipFromTop
            container.animationDurationWithOptions = (0.5, .transitionCrossDissolve)
        }
    }

}


/*
extension ViewController: SecondViewDelegate{
    
    func sendToThirdViewController() {
        container.segueIdentifierReceivedFromParent("third")
    }
    
    
}
*/
