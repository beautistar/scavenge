//
//  SearchVC.swift
//  Scavenge
//
//  Created by Developer on 2/10/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class SearchVC: BaseViewController {

    @IBOutlet weak var lblSearchResult: UILabel!
    @IBOutlet weak var tblSearchList: UITableView!
    @IBOutlet weak var tfSearch: UISearchBar!
    
    var gameList = [GameModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func searchGame(_ keyword: String) {
        
        self.showSearchLoading()
        tfSearch.isUserInteractionEnabled = false
        ApiRequest.search_game(keyword) { (games, message) in
            self.hideLoadingView()
            self.tfSearch.isUserInteractionEnabled = true
            self.lblSearchResult.text = "  \(games.count) Search Result  "
            if message == Const.MSG_SUCCESS {
                self.gameList = games
                if games.count > 0 {
                    self.tblSearchList.reloadData()
                }
            } else {
                self.showAlert(message)
            }
        }
    }
    
    @objc func gotoGameDetail(_ sender: UIButton) {
        
        let gameDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "GameDetailVC") as! GameDetailVC
        gameDetailVC.game = gameList[sender.tag]
        self.navigationController?.pushViewController(gameDetailVC, animated: true)
    }
    
     override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
           self.view.endEditing(true)
    }
}

extension SearchVC: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
       
        self.view.endEditing(true)
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count > 1 {
            searchGame(searchText.lowercased())
        }
    }
}

extension SearchVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return gameList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GameCell") as! GameCell
        cell.entity = gameList[indexPath.row]
        cell.btnJoin.tag = indexPath.row
        cell.btnJoin.addTarget(self, action: #selector(gotoGameDetail(_ :)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return (ScreenRect.width - 30) / 2.0 / 1.3 + 20
        
    }
}
