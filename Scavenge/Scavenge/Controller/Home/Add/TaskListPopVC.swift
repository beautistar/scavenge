//
//  TaskListPopVC.swift
//  Scavenge
//
//  Created by Developer on 2/11/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class TaskListPopVC: BaseViewController {

    @IBOutlet weak var cvCategoryList: UICollectionView!
    @IBOutlet weak var tblTaskList: UITableView!
    
    var taskList = [TaskModel]()
    var categoryList = [CategoryModel]()
    var selectedCategories = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblTaskList.tableFooterView = UIView()
        categoryList.removeAll()
        
        ApiRequest.get_category { (categories, message) in
            self.categoryList = categories
            self.cvCategoryList.reloadData()
        }
        getTasks()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        print("willAppear")
    }
    
    func getTasks() {
        
        guard let game = SelectedGame else {
            
            self.showAlert("Please select a game")
            return
            
        }
        
        self.showLoadingView()
        
        ApiRequest.get_task(game.id) { (tasks, message) in
            self.hideLoadingView()
            if message == Const.MSG_SUCCESS {
                self.taskList = tasks
                self.tblTaskList.reloadData()
            }
        }
    }
    
    @objc func onSelected(_ sender: UIButton) {
        
        let parent = self.parent as! AddTaskVC
        parent.hideTaskList()
        parent.isSelected = true
        parent.task = taskList[sender.tag]
        parent.setupTask()
    }
    
}

extension TaskListPopVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return taskList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskCell") as! TaskCell
        cell.entity = taskList[indexPath.row]
        cell.btnPlus.tag = indexPath.row
        cell.btnPlus.addTarget(self, action: #selector(onSelected(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let parent = self.parent as! AddTaskVC
        parent.hideTaskList()
        parent.isSelected = true
        parent.task = taskList[indexPath.row]
        parent.setupTask()
    }
}

extension TaskListPopVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return categoryList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
        cell.entity = categoryList[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let w = CGFloat(categoryList[indexPath.row].name.count * 6 + 20)
        return CGSize(width: w, height: 30)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        categoryList[indexPath.row].selectState = !categoryList[indexPath.row].selectState
        
        if indexPath.row == 0 {
            for i in 1...categoryList.count-1 {
                categoryList[i].selectState = categoryList[0].selectState
//                selectedCategories.append(categoryList[i].name)
            }
        } else {
            categoryList[0].selectState = false
//            if categoryList[indexPath.row].selectState == false {
//
//                 print(categoryList[indexPath.row].name)
//                 if let index = selectedCategories.firstIndex(of: categoryList[indexPath.row].name) {
//                     print(index)
//                     self.selectedCategories.remove(at: index)
//                 }
//             } else {
//                 self.selectedCategories.append(categoryList[indexPath.row].name)
//            }
            let selectedCount = categoryList.filter{ ($0.selectState == true) }.count
            if selectedCount >= categoryList.count - 1 && categoryList[0].selectState == false {
                categoryList[0].selectState = true
            }
        }

        self.cvCategoryList .reloadData()
    }
}
