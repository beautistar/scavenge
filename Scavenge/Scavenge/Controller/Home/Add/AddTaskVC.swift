//
//  AddTaskVC.swift
//  Scavenge
//
//  Created by Developer on 2/11/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit


class AddTaskVC: BaseViewController {

    @IBOutlet weak var imvTaskPhoto: UIImageView!
    @IBOutlet weak var lblTaskName: UILabel!
    @IBOutlet weak var lblTaskPoint: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblSelectTask: UILabel!
    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var imvVideo: UIImageView!
    @IBOutlet weak var tfComment: UITextField!
    
    var isSelected: Bool = false
    var imgUrl = ""
    var videoUrl = ""
    var task: TaskModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onClose(_: )))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        bgView.addGestureRecognizer(tapGesture)
        bgView.isUserInteractionEnabled = true
        
        setNavigationBarRightButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if task != nil {
            isSelected = true
            setupTask()
        }
        setNavigationBarTitle()
    }
    
    @objc func onClose(_ sender: UITapGestureRecognizer) {
        
        hideTaskList()
    }
    
    func setupTask() {
        lblSelectTask.isHidden = isSelected
        imvTaskPhoto.kf.setImage(with: URL(string: task!.photo_url), placeholder: UIImage(named: "placeholder"))
        lblTaskName.text = task!.name
        lblTaskPoint.text = "\(task!.point)"
    }
    
    func showTaskList() {
        bgView.isHidden = false
        containerView.isHidden = false
    }
    
    func hideTaskList() {
        containerView.isHidden = true
        bgView.isHidden = true
    }    
    
    @IBAction func onShowTasks(_ sender: Any) {
        showTaskList()
    }

    @IBAction func onUploadImage(_ sender: Any) {
        
        AttachmentHandler.shared.showImageAttachmentActionSheet(vc: self)
        
        AttachmentHandler.shared.imagePickedBlock = { (image) in
            
            self.imvPhoto.image = image
            self.imgUrl = saveToFile(image: image, filePath: Const.APPNAME, fileName: "image.png")
            self.videoUrl = ""
            self.imvVideo.image = UIImage()
        }        
    }
    
    @IBAction func onUploadVideo(_ sender: Any) {
        
        AttachmentHandler.shared.showVideoAttachmentActionSheet(vc: self)
        
        AttachmentHandler.shared.videoPickedBlock = { (videoPath) in
            self.videoUrl = videoPath.path!
            let thumbImage = createThumbnailImage(videopath: videoPath as URL)
            self.imvVideo.image = thumbImage
            self.imgUrl = saveToFile(image: thumbImage, filePath: Const.APPNAME, fileName: "thumbnail_image.png")
            self.imvPhoto.image = UIImage()
        }
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: false)
    }
    
    @IBAction func onAddPost(_ sender: Any) {
        
        if task == nil {
            self.showAlert("Please select a task")
            return
        }
        
        if (imgUrl == "") && (videoUrl == "") && tfComment.text!.count == 0 {
            self.showAlert("Please add information to post")
            return
        }        
        
        self.showLoadingView()
        
        ApiRequest.add_post(task!.id, comment: tfComment.text!, photoUrl: imgUrl, videoUrl: videoUrl) { (message) in
            self.hideLoadingView()
            if message == Const.MSG_SUCCESS {
                self.navigationController?.popViewController(animated: true)
            } else {
                self.showAlert("Fail to add post. please try again later")
            }
        }
    }
}
