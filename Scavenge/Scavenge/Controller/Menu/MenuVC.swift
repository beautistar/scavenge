//
//  MenuViewController.swift
//  Pixil
//
//  Created by Developer on 7/25/19.
//  Copyright © 2019 Ankhbayar Batbaatar. All rights reserved.
//

import UIKit
import Kingfisher

enum LeftMenu: Int {
    case main = 0
    case post
    case task
    case game
    case setting
    case profile
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}

class MenuVC: BaseViewController, UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate, LeftMenuProtocol {
   
    @IBOutlet weak var tblmenu: UITableView!
    @IBOutlet weak var logout: UIButton!
    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imvGamePhoto: UIImageView!
    @IBOutlet weak var lblGameName: UILabel!
    @IBOutlet weak var lblEmpty: UILabel!
    
    var menus = ["ACTIVITY", "SCOREBOARD", "CHANGE GAME", "OVERVIEW", "PROFILE"]
    var icons = ["postview_icon", "taskview_icon", "change_icon", "settings_icon", "profile_icon"]
    
    var mainVC: UIViewController!    
    var taskVC: UIViewController!
    var gameVC: UIViewController!
    var settingVC: UIViewController!
    var profileVC: UIViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
        
        imvPhoto.layer.borderWidth = 2.0
        imvPhoto.layer.borderColor =  UIColor.white.cgColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let gradient = CAGradientLayer()
        gradient.frame = self.view.bounds
        gradient.colors = [UIColor(hex: "0a84b7").cgColor, UIColor(hex: "073778").cgColor]
        gradient.locations = [0.0, 1.0]
        self.view.layer.insertSublayer(gradient, at: 0)
        
        super.viewWillAppear(animated)
        
        if let user = currentUser {
            lblName.text = user.name
            imvPhoto.kf.setImage(
                with: URL(string: user.photo_url),
            placeholder: UIImage(named: "profile"))
        }        
        
        logout.layer.borderColor = UIColor.white.cgColor
        logout.layer.cornerRadius = 20
        logout.layer.borderWidth = 1
        
        setGameTitle()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func setGameTitle() {
        if SelectedGame == nil {
            imvGamePhoto.isHidden = true
            lblGameName.isHidden = true
            lblEmpty.isHidden = false
        } else {
            imvGamePhoto.isHidden = false
            lblGameName.isHidden = false
            lblEmpty.isHidden = true
            imvGamePhoto.kf.setImage(with: URL(string: SelectedGame!.photo_url))
            lblGameName.text = SelectedGame?.name
        }
    }
    
    func initView() {
        
        tblmenu.tableFooterView = UIView()

        let mainVC = storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
        self.mainVC = UINavigationController(rootViewController: mainVC)
        
        let taskVC = self.storyboard?.instantiateViewController(withIdentifier: "ScoreboardVC") as! ScoreboardVC
        self.taskVC = UINavigationController(rootViewController: taskVC)
        
        let gameVC = self.storyboard?.instantiateViewController(withIdentifier: "GameVC") as! GameVC       
        self.gameVC = UINavigationController(rootViewController: gameVC)
        
        let settingVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        settingVC.delegate = self
        self.settingVC = UINavigationController(rootViewController: settingVC)
        
        let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        profileVC.delegate = self
        self.profileVC = UINavigationController(rootViewController: profileVC)

    }

    // MARK: - left menu protocol
    func changeViewController(_ menu: LeftMenu) {
        
        switch menu {

        case .main:
            from = .main
            self.slideMenuController()?.changeMainViewController(self.mainVC, close: true)
        case .post:
            from = .post
            self.slideMenuController()?.changeMainViewController(self.mainVC, close: true)
        case .task:
            from = .task
            self.slideMenuController()?.changeMainViewController(self.taskVC, close: true)
        case .game:
            popCount = 2
            from = .game
            self.slideMenuController()?.changeMainViewController(self.gameVC, close: true)
        case .setting:
            from = .setting
            self.slideMenuController()?.changeMainViewController(self.settingVC, close: true)
        case .profile:
            from = .setting
            self.slideMenuController()?.changeMainViewController(self.profileVC, close: true)
        }
    }
    
    
    // MARK: - Actions
    @IBAction func onLogOut(_ sender: Any) {
        
        //let user = UserModel()
        //currentUser = user
        
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        
        let loginNavVC = self.storyboard?.instantiateViewController(withIdentifier: "loginNav") as! UINavigationController
        UIApplication.shared.keyWindow?.rootViewController = loginNavVC
        
    }


    // MARK: -  UITableView Delegate, Datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return menus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell") as! MenuCell
        
        cell.lblName.text = menus[indexPath.row]
        cell.imvIcon.image = UIImage(named: icons[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.row+1) {
            self.changeViewController(menu)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tblmenu == scrollView {
            
        }
    }
}
