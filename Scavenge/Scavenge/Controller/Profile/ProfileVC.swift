//
//  ProfileVC.swift
//  Scavenge
//
//  Created by Developer on 1/29/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class ProfileVC: BaseViewController {

    weak var delegate: LeftMenuProtocol?
    var imgUrl = ""
    
    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imvPhoto.layer.borderWidth = 2.0
        imvPhoto.layer.borderColor =  Const.COLOR_MAIN?.cgColor
        
        setUserData()
    }
      
      
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
      
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            guard let vc = (self.slideMenuController()?.mainViewController as? UINavigationController)?.topViewController else {
                return
            }
            if vc.isKind(of: ProfileVC.self)  {
                self.slideMenuController()?.removeLeftGestures()
                self.slideMenuController()?.removeRightGestures()
            }
        })
    }
    
    func setUserData() {
        
        if let user = currentUser {
            
            imvPhoto.kf.setImage(with: URL(string: user.photo_url), placeholder: UIImage(named: "placeholder"))
            lblName.text = user.name
            tfName.text = user.name
            tfEmail.text = user.email
            tfPassword.text = user.password
            tfPhone.text = user.phone_no
        }        
    }
    
    func isValid() -> Bool {
        
        if tfName.text?.count == 0 {
            self.showAlertDialog(title: Const.APPNAME, message: Const.CHECK_NAME_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfEmail.text?.count == 0 {
            self.showAlertDialog(title: Const.APPNAME, message: Const.CHECK_EMAIL_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        if !CommonUtils.isValidEmail(tfEmail.text!) {
            self.showAlertDialog(title: Const.APPNAME, message: Const.CHECK_VAILD_EMAIL, positive: Const.OK, negative: nil)
            return false
        }
        if tfPhone.text?.count == 0 {
            self.showAlertDialog(title: Const.APPNAME, message: Const.CHECK_EMPTY_PHONE, positive: Const.OK, negative: nil)
            return false
        }
        if tfPassword.text?.count == 0 {
            self.showAlertDialog(title: Const.APPNAME, message: Const.CHECK_PASSWORD, positive: Const.OK, negative: nil)
            return false
        }
        
        return true
    }
    
    func editUser() {
        
        let user = UserModel()
        user.user_id = currentUser!.user_id
        user.name = tfName.text!
        user.email = tfEmail.text!
        user.password = tfPassword.text!
        user.phone_no = tfPhone.text!
        
        self.showLoadingView()
        
        ApiRequest.edit_user(user) { (message) in
            
            if message == Const.MSG_SUCCESS {
                if self.imgUrl == "" {
                    self.hideLoadingView()
                    self.showAlert("Your profile is updated successfully")
                } else {
                    self.uploadPhoto()
                }
            } else {
                self.hideLoadingView()
                self.showAlert(message)
            }
        }
    }
    
    func uploadPhoto() {
        
        ApiRequest.update_photo(imgUrl) { (message) in
            self.hideLoadingView()
            if message == Const.MSG_SUCCESS {
                self.showAlert("Your profile is updated successfully")
            } else {
                self.showAlert(message)
            }
        }
    }
    
    @IBAction func onBack(_ sender: Any) {
        delegate?.changeViewController(.main)
    }
    
    @IBAction func onUploadImage(_ sender: Any) {
        
        AttachmentHandler.shared.showImageAttachmentActionSheet(vc: self)
        
        AttachmentHandler.shared.imagePickedBlock = { (image) in
            
            self.imvPhoto.image = image
            
            self.imgUrl = saveToFile(image: image, filePath: Const.APPNAME, fileName: "image.png")
            print("imgUrl", self.imgUrl)
        }
    }
    
    @IBAction func onUpdateProfile(_ sender: Any) {
        
        if isValid() {
            editUser()
        }
    }
}
