//
//  SettingVC.swift
//  Scavenge
//
//  Created by Developer on 1/29/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit
import MessageUI

class SettingVC: BaseViewController, MFMailComposeViewControllerDelegate {

    weak var delegate: LeftMenuProtocol?
    
    @IBOutlet weak var txvNote: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
      
      
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
      
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            guard let vc = (self.slideMenuController()?.mainViewController as? UINavigationController)?.topViewController else {
                return
            }
            if vc.isKind(of: SettingVC.self)  {
                self.slideMenuController()?.removeLeftGestures()
                self.slideMenuController()?.removeRightGestures()
            }
        })
    }
    
    @IBAction func onBack(_ sender: Any) {
        delegate?.changeViewController(.main)
    }
    @IBAction func onContactUs(_ sender: Any) {
        
         if txvNote.text?.count == 0 {
             self.showAlert("Please write something..")
             return
         }
         if MFMailComposeViewController.canSendMail() {
             let mail = MFMailComposeViewController()
             mail.mailComposeDelegate = self
             mail.setToRecipients([Const.SUPPORT_MAIL])
             mail.setSubject("Scavenge")
            mail.setMessageBody(txvNote.text! + "<br>" + currentUser!.name, isHTML: true)
             
             present(mail, animated: true)
         } else {
             // show failure alert
             self.showAlert("You need to setup your email in setting")
         }
        
    }
}
