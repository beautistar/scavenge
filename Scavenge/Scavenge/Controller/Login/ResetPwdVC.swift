//
//  ResetPwdVC.swift
//  Scavenge
//
//  Created by Developer on 2/29/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class ResetPwdVC: BaseViewController {

    @IBOutlet weak var tfNewPassword: UITextField!
    @IBOutlet weak var tfRePassword: UITextField!
    @IBOutlet weak var btnResetPwd: UIButton!
    
    var email = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnResetPwd.layer.borderColor = Const.COLOR_MAIN?.cgColor
    }

    @IBAction func onBack(_ sender: Any) {        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func onResetPassword(_ sender: Any) {
        
        if isValid() {
            
            resetPassword()
        }
    }
    
    func isValid() -> Bool {
        
        if tfNewPassword.text?.count == 0 {
            self.showAlert("Please input new password")
            return false
        }
        
        if tfRePassword.text?.count == 0 {
            self.showAlert("Please input confirm password")
            return false
        }
        
        if tfNewPassword.text != tfRePassword.text {
            self.showAlert("Password does not match")
            return false
        }
        
        
        return true
    }
    
    func resetPassword() {
        
        self.showLoadingView()
        
        ApiRequest.reset_pwd(email: email, password: tfNewPassword.text!) { (message) in
            
            self.hideLoadingView()
            if message == Const.MSG_SUCCESS {
                self.showAlert(title: Const.APPNAME, message: "You reset your password successfully.\nYou can now log in with new password", okButtonTitle: "OK", cancelButtonTitle: nil) {
                    self.navigationController?.popToRootViewController(animated: true)
                }
            } else {
                self.showAlert(message)
            }
        }
    }
    
}
