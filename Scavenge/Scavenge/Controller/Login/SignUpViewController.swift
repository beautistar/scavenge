//
//  SignUpViewController.swift
//  Scavenge
//
//  Created by Developer on 1/27/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import SwiftyJSON
import Firebase
import GoogleSignIn

class SignUpViewController: BaseViewController {

    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var imvCheck: UIImageView!
    
    var user = UserModel()
    var imgUrl = ""
    var isChecked = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imvPhoto.layer.borderWidth = 2.0
        imvPhoto.layer.borderColor =  Const.COLOR_MAIN?.cgColor

    }
    
    //MARK:- Private actions
    
    @IBAction func onUploadImage(_ sender: Any) {
        
        AttachmentHandler.shared.showImageAttachmentActionSheet(vc: self)
        
        AttachmentHandler.shared.imagePickedBlock = { (image) in
            
            self.imvPhoto.image = image
            
            self.imgUrl = saveToFile(image: image, filePath: Const.APPNAME, fileName: "image.png")
            print("imgUrl", self.imgUrl)
        }
    }
    
    @IBAction func onSIgnUp(_ sender: Any) {
        
        if isValid() {
            doSignupProcess()
        }
    }
    
    @IBAction func onFBLogin(_ sender: Any) {
        
        if (!isChecked) {
            self.showAlert("Please agree to terms and condition & privacy policy")
            return
        }
        
        let fbLoginManager : LoginManager = LoginManager()
        fbLoginManager.logIn(permissions: ["public_profile", "email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : LoginManagerLoginResult = result!
                if(fbloginresult.grantedPermissions.contains("email")) {
                    self.getFBUserData()
                    fbLoginManager.logOut()
                }
            } else {
                print("Login failed")
            }
        }
    }
    
    @IBAction func onGoogleLogin(_ sender: Any) {
        
        if (!isChecked) {
            self.showAlert("Please agree to terms and condition & privacy policy")
            return
        }
        
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func onAgreeTerm(_ sender: Any) {
        if isChecked {
            imvCheck.image = UIImage(named: "unchecked")
        } else {
            imvCheck.image = UIImage(named: "checked")
        }
        isChecked = !isChecked
    }
    
    @IBAction func onGoTerms(_ sender: Any) {
        
    }
    
    //MARK: - Private method
    
    func isValid() -> Bool {
        
        if imgUrl == "" {
            self.showAlert("Please add your photo")
            return false
        }
        
        if tfName.text?.count == 0 {
            self.showAlertDialog(title: Const.APPNAME, message: Const.CHECK_NAME_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfEmail.text?.count == 0 {
            self.showAlertDialog(title: Const.APPNAME, message: Const.CHECK_EMAIL_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        if !CommonUtils.isValidEmail(tfEmail.text!) {
            self.showAlertDialog(title: Const.APPNAME, message: Const.CHECK_VAILD_EMAIL, positive: Const.OK, negative: nil)
            return false
        }
        if tfPhone.text?.count == 0 {
            self.showAlertDialog(title: Const.APPNAME, message: Const.CHECK_EMPTY_PHONE, positive: Const.OK, negative: nil)
            return false
        }
        if tfPassword.text?.count == 0 {
            self.showAlertDialog(title: Const.APPNAME, message: Const.CHECK_PASSWORD, positive: Const.OK, negative: nil)
            return false
        }
        
        if (!isChecked) {
            self.showAlert("Please agree to terms and condition & privacy policy")
            return false
        }
        
        return true
    }
    
    func doSignupProcess() {
        
        user.name = tfName.text!
        user.email = tfEmail.text!
        user.phone_no = tfPhone.text!
        user.password = tfPassword.text!
        user.login_type = LoginType.email.rawValue
        user.photo_url = imgUrl
        
        self.showLoadingView()
        ApiRequest.register(user) { (message) in
            self.hideLoadingView()
            if message == Const.MSG_SUCCESS {
                let token = currentUser?.token
                ApiRequest.register_token(token!) { (msg) in }
                self.createMenuView()
            } else {
                self.showAlertDialog(title: Const.APPNAME, message: message, positive: Const.OK, negative: nil)
            }
        }
    }
    
    func doSocialLogin(_ social_type: String) {

        user.login_type = social_type
        self.showLoadingView()
        ApiRequest.social_login(user) { (code, message) in
            self.hideLoadingView()
            if code == Const.CODE_SUCCESS || code == Const.CODE_203 {
                let token = currentUser?.token
                ApiRequest.register_token(token!) { (msg) in }
                self.createMenuView()
            } else {
                self.showAlertDialog(title: Const.APPNAME, message: message, positive: Const.OK, negative: nil)
            }
        }
    }
    
    func getFBUserData(){
        
        if((AccessToken.current) != nil) {
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, email, first_name, last_name"]).start(completionHandler: { (connection, result, error) -> Void in
                
                if (error == nil) {
                    
                    let jsonResponse = JSON(result!)
                    
                    print("jsonResponse", jsonResponse)
                    
                    let id = jsonResponse["id"].stringValue
                    self.user.password = id
                    self.user.email = jsonResponse["email"].string ?? String(format: "%@@facebook.com", id)
                    self.user.name = jsonResponse["name"].string ?? "unknown"
                    self.user.photo_url = "https://graph.facebook.com/" + id + "/picture?type=large"
                    
                    print("FB result", self.user.name, self.user.email, id, self.user.photo_url)
                    
                    self.doSocialLogin(LoginType.facebook.rawValue)
                    
                } else {
                    // TODO:  Exeption  ****
                    print(error!)
                }
            })
        } else {
            print("token is null")
        }
    }
}

extension SignUpViewController: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
      
        if error == nil {
            
            guard let user = user else {return}
            print("GIDSignIn User", user)
            
            let id = user.userID!
            self.user.password = id
            self.user.email = user.profile.email ?? String(format: "%@@google.com", id)
            self.user.name = user.profile.name ?? "unknown"
            self.user.photo_url = user.profile.imageURL(withDimension: 300)!.description
            
            print("Google result", self.user.name, self.user.email, id, self.user.photo_url)
            doSocialLogin(LoginType.google.rawValue)
        } else {
            print("GIDSignIn Error:--  ", error!.localizedDescription)
        }
    }

    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        viewController.modalPresentationStyle = .fullScreen
        present(viewController, animated: true) {}
    }
}
