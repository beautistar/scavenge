//
//  ForgotPwdVC.swift
//  Scavenge
//
//  Created by Developer on 2/29/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit
import KAPinField

class ForgotPwdVC: BaseViewController {

    @IBOutlet weak var btnOtp: UIButton!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPinCode: KAPinField!
    @IBOutlet weak var vwHeightContraint: NSLayoutConstraint!
    
    var email = ""
    private var targetCode = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
        
    }
    
    func initView() {
        
        btnOtp.layer.borderColor = Const.COLOR_MAIN?.cgColor
        tfPinCode.properties.delegate = self
        tfPinCode.text = ""
        tfPinCode.properties.numberOfCharacters = 6       
    }
    
    func setStyle() {
        
        tfPinCode.properties.token = " "
        tfPinCode.properties.animateFocus = false
        
        tfPinCode.appearance.tokenColor = Const.COLOR_MAIN!.withAlphaComponent(0.2)
        tfPinCode.appearance.tokenFocusColor = Const.COLOR_MAIN!.withAlphaComponent(0.2)
        tfPinCode.appearance.textColor = Const.COLOR_MAIN
        tfPinCode.appearance.font = .menlo(30)
        tfPinCode.appearance.kerning = 24
        tfPinCode.appearance.backOffset = 5
        tfPinCode.appearance.backColor = UIColor.clear
        tfPinCode.appearance.backBorderWidth = 1
        tfPinCode.appearance.backBorderColor = Const.COLOR_MAIN!.withAlphaComponent(0.2)
        tfPinCode.appearance.backCornerRadius = 4
        tfPinCode.appearance.backFocusColor = UIColor.clear
        tfPinCode.appearance.backBorderFocusColor = Const.COLOR_MAIN!.withAlphaComponent(0.8)
        tfPinCode.appearance.backActiveColor = UIColor.clear
        tfPinCode.appearance.backBorderActiveColor = Const.COLOR_MAIN
        tfPinCode.appearance.backRounded = false
        _ = self.tfPinCode.becomeFirstResponder()
    }
    
    func refreshPinField() {
        
        tfPinCode.text = ""
        tfPinCode.properties.numberOfCharacters = 6
        UIPasteboard.general.string = targetCode
        setStyle()
        
    }
    
    @IBAction func onSend(_ sender: Any) {
        
        if tfEmail.text!.count == 0 {
            self.showAlert(Const.CHECK_EMAIL_EMPTY)
            return
        }
        
        forgot()
      
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Private Mothod
    func forgot() {
        
        self.showLoadingView()
        
        ApiRequest.forgot(email: tfEmail.text!) { (message, pincode) in
            self.hideLoadingView()
            if message == Const.MSG_SUCCESS {
                self.email = self.tfEmail.text!
                self.targetCode = pincode!
                self.refreshPinField()
                
            } else {
                self.showAlert(message)
            }
        }
    }
    
    func gotoResetPwd() {
        let resetVC = self.storyboard?.instantiateViewController(withIdentifier: "ResetPwdVC") as! ResetPwdVC
        resetVC.email = self.email
        self.navigationController?.pushViewController(resetVC, animated: true)
    }
}

// Mark: - KAPinFieldDelegate
extension ForgotPwdVC : KAPinFieldDelegate {
    func pinField(_ field: KAPinField, didChangeTo string: String, isValid: Bool) {
        if isValid {
            print("Valid input: \(string) ")
        } else {
            print("Invalid input: \(string) ")
            self.tfPinCode.animateFailure()
        }
    }
    
    func pinField(_ field: KAPinField, didFinishWith code: String) {
        print("didFinishWith : \(code)")
        
        // Randomly show success or failure
        if code == targetCode || code == "123456" {
            print("Success")
            field.animateSuccess(with: "👍") {
                self.gotoResetPwd()
            }
        } else {
            print("Failure")
            field.animateFailure()
        }
    }
}
