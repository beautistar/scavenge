//
//  SignInViewController.swift
//  Scavenge
//
//  Created by Developer on 1/27/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import SwiftyJSON
import Firebase
import GoogleSignIn

class SignInViewController: BaseViewController {
    
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    
    var user = UserModel()
    var email = ""
    var password = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let email = UserDefaults.standard.string(forKey: Const.KEY_EMAIL) {
            if let password = UserDefaults.standard.string(forKey: Const.KEY_PASSWORD) {
                if let type = UserDefaults.standard.string(forKey: Const.KEY_TYPE) {
                    
                    self.email = email
                    self.password = password
                    if type == LoginType.email.rawValue {
                        self.doLoginProcess()
                    } else {
                        self.doSocialLogin(type)
                    }
                }
            }
        }
    }
    
    @IBAction func onSignIn(_ sender: Any) {
    
        if isValid() {
            email = tfEmail.text!
            password = tfPassword.text!
            doLoginProcess()
        }
    }
    
    @IBAction func onFbLogin(_ sender: Any) {
        
        let fbLoginManager : LoginManager = LoginManager()
        fbLoginManager.logIn(permissions: ["public_profile", "email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : LoginManagerLoginResult = result!
                if(fbloginresult.grantedPermissions.contains("email")) {
                    self.getFBUserData()
                    fbLoginManager.logOut()
                }
            } else {
                print("Login failed")
            }
        }
    }
    @IBAction func onGoogleLogin(_ sender: Any) {
        
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    //MARK:- private method
    func isValid() -> Bool {
        
        if tfEmail.text?.count == 0 {
            self.showAlertDialog(title: Const.APPNAME, message: Const.CHECK_EMAIL_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        if !CommonUtils.isValidEmail(tfEmail.text!) {
            self.showAlertDialog(title: Const.APPNAME, message: Const.CHECK_VAILD_EMAIL, positive: Const.OK, negative: nil)
            return false
        }
        if tfPassword.text?.count == 0 {
            self.showAlertDialog(title: Const.APPNAME, message: Const.CHECK_PASSWORD, positive: Const.OK, negative: nil)
            return false
        }
        
        return true
    }
    
    func getFBUserData(){
        
        if((AccessToken.current) != nil) {
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, email, first_name, last_name"]).start(completionHandler: { (connection, result, error) -> Void in
                
                if (error == nil) {
                    
                    let jsonResponse = JSON(result!)
                    
                    print("jsonResponse", jsonResponse)
                    
                    let id = jsonResponse["id"].stringValue
                    self.user.password = id
                    self.user.email = jsonResponse["email"].string ?? String(format: "%@@facebook.com", id)
                    self.user.name = jsonResponse["name"].string ?? "unknown"
                    self.user.photo_url = "https://graph.facebook.com/" + id + "/picture?type=large"
                    
                    print("FB result", self.user.name, self.user.email, id, self.user.photo_url)
                    
                    self.doSocialLogin(LoginType.facebook.rawValue)
                    
                } else {
                    // TODO:  Exeption  ****
                    print(error!)
                }
            })
        } else {
            print("token is null")
        }
    }
    
    func doSocialLogin(_ social_type: String) {

        user.login_type = social_type
        self.showLoadingView()
        ApiRequest.social_login(user) { (code, message) in
            self.hideLoadingView()
            if code == Const.CODE_SUCCESS || code == Const.CODE_203 {
                let token = currentUser?.token
                ApiRequest.register_token(token!) { (msg) in }
                self.createMenuView()
            } else {
                self.showAlertDialog(title: Const.APPNAME, message: message, positive: Const.OK, negative: nil)
            }
        }
    }
    
    func doLoginProcess() {
        
        self.showLoadingView()
        
        ApiRequest.login(email: email, password: password) { (message) in
            
            self.hideLoadingView()
            
            if message == Const.MSG_SUCCESS {
                let token = currentUser?.token
                ApiRequest.register_token(token!) { (msg) in }
                self.createMenuView()
            } else {
                self.showAlertDialog(title: Const.APPNAME, message: message, positive: Const.OK, negative: nil)
            }
        }
    }
}

extension SignInViewController: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
      
        if error == nil {
            
            guard let user = user else {return}
            print("GIDSignIn User", user)
            
            let id = user.userID!
            self.user.password = id
            self.user.email = user.profile.email ?? String(format: "%@@google.com", id)
            self.user.name = user.profile.name ?? "unknown"
            self.user.photo_url = user.profile.imageURL(withDimension: 300)!.description
            
            print("Google result", self.user.name, self.user.email, id, self.user.photo_url)
            doSocialLogin(LoginType.google.rawValue)
        } else {
            print("GIDSignIn Error:--  ", error!.localizedDescription)
        }
    }

    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        viewController.modalPresentationStyle = .fullScreen
        present(viewController, animated: true) {}
    }
}
