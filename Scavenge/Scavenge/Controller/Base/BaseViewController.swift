
import UIKit
import ALLoadingView

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func createMenuView() {
        
        let mainViewController = storyboard!.instantiateViewController(withIdentifier: "MainVC") as! MainVC
        let leftViewController = storyboard!.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        
        UINavigationBar.appearance().tintColor = Const.COLOR_MAIN
        
        leftViewController.gameVC = nvc
        
        let slideMenuController = ExSlideMenuController(mainViewController: nvc, leftMenuViewController: leftViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController

        UIApplication.shared.keyWindow?.rootViewController = slideMenuController
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
       
    }
    
    internal func showAlert(title: String?, message: String?, okButtonTitle: String, cancelButtonTitle: String?, okClosure: (() -> Void)?) {
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        let yesAction = UIAlertAction(title: okButtonTitle, style: .default, handler: { (action: UIAlertAction) in
            
            if okClosure != nil {
                okClosure!()
            }
        })
        alertController.addAction(yesAction)
        if cancelButtonTitle != nil {
            let noAction = UIAlertAction(title: cancelButtonTitle, style: .cancel, handler: { (action: UIAlertAction) in
                
            })
            alertController.addAction(noAction)
        }
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func addPadding(_ textfield: UITextField) {        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: textfield.frame.height))
        textfield.leftView = paddingView
        textfield.leftViewMode = .always
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func showAlertDialog(title: String!, message: String!, positive: String?, negative: String?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if (positive != nil) {
            
            alert.addAction(UIAlertAction(title: positive, style: .default, handler: nil))
        }
        
        if (negative != nil) {
            
            alert.addAction(UIAlertAction(title: negative, style: .default, handler: nil))
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func showAlert(_ message: String) {
        
        showAlertDialog(title: Const.APPNAME, message: message, positive: Const.OK, negative: nil)
    }
    
    func showLoadingView() {
        showLoading(true)
    }
    
    func hideLoadingView() {
        showLoading(false)
    }
    
    func showLoading(_ status: Bool)  {
      
        if status {
            ALLoadingView.manager.messageText = ""
            ALLoadingView.manager.animationDuration = 0.0
            ALLoadingView.manager.showLoadingView(ofType: .messageWithIndicator)
            return
        }
        ALLoadingView.manager.hideLoadingView()    
    }
    
    func showSearchLoading() {
        ALLoadingView.manager.resetToDefaults()
        ALLoadingView.manager.messageText = "Searching..."
        ALLoadingView.manager.animationDuration = 0.0
        ALLoadingView.manager.windowRatio = 0.4
        ALLoadingView.manager.showLoadingView(ofType: .messageWithIndicator, windowMode: .windowed)
        
    }
    
    @IBAction func onHome(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func onAdd(_ sender: Any) {
        
        if SelectedGame == nil {
            self.showAlert("Please select a game")
        } else {
            let addTaskVC = self.storyboard?.instantiateViewController(withIdentifier: "AddTaskVC") as! AddTaskVC
            self.navigationController?.pushViewController(addTaskVC, animated: false)
        }
    }
    
    @IBAction func onSearch(_ sender: Any) {
        if SelectedGame == nil {
            self.showAlert("Please select a game")
        } else {
            let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
            self.navigationController?.pushViewController(searchVC, animated: false)
        }
    }
    
    @IBAction func onGameHome(_ sender: Any) {
        
        createMenuView()
    }
}

