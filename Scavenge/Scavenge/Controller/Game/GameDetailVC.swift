//
//  GameDetailVC.swift
//  Scavenge
//
//  Created by Developer on 2/14/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class GameDetailVC: BaseViewController {

    @IBOutlet weak var imvGamePhoto: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var imvOwnerProfile: UIImageView!
    @IBOutlet weak var lblOwnerName: UILabel!
    @IBOutlet weak var lblTotalTasks: UILabel!
    @IBOutlet weak var cvTaskList: UICollectionView!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var selectButtonView: UIView!
    @IBOutlet weak var btnJoin: UIButton!
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var lbldescription: UILabel!
    
    var game = GameModel()
    var taskList = [TaskModel]()
    var previousVC = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
        getTasks()
        setButton()      
    }
    
    func initView() {
        imvGamePhoto.kf.setImage(with: URL(string: game.photo_url), placeholder: UIImage(named: "placeholder"))
        lblName.text = game.name
        lblStartDate.text = formatedDate(game.start_date)
        lblEndDate.text = formatedDate(game.end_date)
        lblComment.text = "\(game.comment_count) Comments"
        lbldescription.text = game.description
    }
    
    func getTasks() {
        
        self.showLoadingView()
        
        ApiRequest.get_task(game.id) { (tasks, message) in
            self.hideLoadingView()
            if message == Const.MSG_SUCCESS {
                self.lblTotalTasks.text = "\(tasks.count) TASKS"
                self.taskList = tasks
                self.cvTaskList.reloadData()
            }
        }
    }
    
    func setButton() {
        
        if sendVC == "join" {
            selectButtonView.isHidden = true
            btnJoin.isHidden = false
        } else {
            selectButtonView.isHidden = false
            btnJoin.isHidden = true
            setSelectButton()
        }
    }
    
    func setSelectButton() {
        
        if SelectedGame?.id == game.id {
            btnSelect.backgroundColor = UIColor.lightGray
        }
    }
    func joinGame() {
        
        self.showLoadingView()
        ApiRequest.join_game(game.id) { (msg) in
            self.hideLoadingView()
            if msg == Const.MSG_SUCCESS {
                self.showAlert(title: "Success", message: "You succeed to join to game", okButtonTitle: Const.OK, cancelButtonTitle: nil) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    func leaveGame() {
        
        self.showLoadingView()
        ApiRequest.leave_game(game.id) { (msg) in
            self.hideLoadingView()
            if msg == Const.MSG_SUCCESS {
                self.showAlert(title: "Success", message: "You succeed to leave to game", okButtonTitle: Const.OK, cancelButtonTitle: nil) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    @IBAction func onShowImage(_ sender: Any) {
        let imageVC = self.storyboard?.instantiateViewController(withIdentifier: "ImageVC") as! ImageVC
        imageVC.urlString = game.photo_url
        imageVC.modalTransitionStyle = .crossDissolve
        imageVC.modalPresentationStyle = .fullScreen
        self.present(imageVC, animated: true, completion: nil)
    }
    
    @IBAction func onJoinGame(_ sender: Any) {
        joinGame()
    }
    
    @IBAction func onClickSelect(_ sender: Any) {
        if SelectedGame?.id == game.id {
            showAlertDialog(title: "Warning!", message: "You selected this game.", positive: Const.OK, negative: nil)
        }
        self.showAlert(title: Const.APPNAME, message: "Are you sure want to select this game?", okButtonTitle: Const.YES, cancelButtonTitle: Const.NO) {
            SelectedGame = myGameList[senderID]
            //self.navigationController?.popViewController(animated: true)
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - popCount], animated: true)
        }
    }
    
    @IBAction func onClickLeaveGame(_ sender: Any) {
        leaveGame()
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)        
    }
}

extension GameDetailVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.taskList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TaskCollectionViewCell", for: indexPath) as! TaskCollectionViewCell
        cell.entity = self.taskList[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let w = (ScreenRect.width - 50)/3.0
        let h = collectionView.frame.size.height/2 - 7.5
        return CGSize(width: w, height: h)
    }
}
