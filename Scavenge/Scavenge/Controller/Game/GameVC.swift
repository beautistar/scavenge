//
//  GameVC.swift
//  Scavenge
//
//  Created by Developer on 3/1/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

var sendVC = ""
var senderID = 0
var myGameList = [GameModel]()

class GameVC: BaseViewController {
    
    @IBOutlet weak var tblGameList: UITableView!
    @IBOutlet weak var btnActiveGame: UIButton!
    @IBOutlet weak var btnCurrentGame: UIButton!

    var isActiveGames: Bool = false
    var activeGameList = [GameModel]()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setNavigationBarItem()
        setNavigationBarRightButton()
        
        btnActiveGame.setTitleColor(.black, for: .normal)
        btnCurrentGame.setTitleColor(Const.COLOR_MAIN, for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getGame()
    }
    
    @IBAction func onSwitch(_ sender: Any) {
        
        let btnSwitch: UIButton = sender as! UIButton
        if btnSwitch.tag == 0 {
            isActiveGames = true
            btnActiveGame.setTitleColor(Const.COLOR_MAIN, for: .normal)
            btnCurrentGame.setTitleColor(.black, for: .normal)
        } else {
            isActiveGames = false
            btnActiveGame.setTitleColor(.black, for: .normal)
            btnCurrentGame.setTitleColor(Const.COLOR_MAIN, for: .normal)
        }
        
        tblGameList.reloadData()
    }
    
    func getGame() {
        
        self.showLoadingView()
       
        ApiRequest.get_game { (games, message) in
            self.hideLoadingView()
            
            if message == Const.MSG_SUCCESS {
                self.activeGameList = games.filter {($0.is_joined == "no")}
                myGameList = games.filter {($0.is_joined == "yes")}
                self.tblGameList.reloadData()
            }
        }
    }
    
    @objc func gotoGameDetail(_ sender: UIButton) {
        
        sendVC = "join"
        let gameDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "GameDetailVC") as! GameDetailVC
        gameDetailVC.game = self.activeGameList[sender.tag]
        self.navigationController?.pushViewController(gameDetailVC, animated: true)
    }
}

extension GameVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isActiveGames {
            return activeGameList.count
        } else {
            return myGameList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GameCell") as! GameCell
        cell.imvPhoto.image = UIImage(named: "dummy\(indexPath.row+1)")
        if isActiveGames {
            cell.btnJoin.isHidden = false
            cell.btnJoin.tag = indexPath.row
            cell.entity = activeGameList[indexPath.row]
        } else {
            cell.entity = myGameList[indexPath.row]
            cell.btnJoin.isHidden = true
        }
        
        cell.btnJoin.tag = indexPath.row
        cell.btnJoin.addTarget(self, action: #selector(gotoGameDetail(_ :)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return (ScreenRect.width - 30) / 2.0 / 1.3 + 20
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if !isActiveGames {
           
            sendVC = "select"
            senderID = indexPath.row
            let gameDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "GameDetailVC") as! GameDetailVC            
            gameDetailVC.game = myGameList[indexPath.row]
            self.navigationController?.pushViewController(gameDetailVC, animated: true)
//            self.showAlert(title: Const.APPNAME, message: "Are you sure want to select this game?", okButtonTitle: Const.YES, cancelButtonTitle: Const.NO) {
//                SelectedGame = self.myGameList[indexPath.row]
//                self.navigationController?.popViewController(animated: true)
//            }
        }
    }
}
