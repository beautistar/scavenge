//
//  CommonUtils.swift
//  Pixil
//
//  Created by Developer on 8/19/19.
//  Copyright © 2019 Ankhbayar Batbaatar. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import AVKit

enum From {
    case signup
    case home
    case main
    case post
    case task
    case game
    case profile
    case setting
    case back
    case other
}

enum LoginType: String {
    case email = "email"
    case facebook = "facebook"
    case google = "google"
}

var SelectedGame: GameModel?

var ScreenRect : CGRect {
    return UIScreen.main.bounds
}

var categoryNames = ["#All", "#Challenges", "#Dares", "#Accomplishments", "#Things observed"]

var from = From.home

var currentUser : UserModel? {
    didSet {
        if let user = currentUser {
            UserDefaults.standard.set(user.name, forKey: Const.KEY_NAME)
            UserDefaults.standard.set(user.user_id, forKey: Const.KEY_USERID)
            UserDefaults.standard.set(user.email, forKey: Const.KEY_EMAIL)
            UserDefaults.standard.set(user.password, forKey: Const.KEY_PASSWORD)
            UserDefaults.standard.set(user.login_type, forKey: Const.KEY_TYPE)
        }
    }
}

class CommonUtils {
    
    static func isValidEmail(_ email: String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
}
// "2020-12-20" to "20 Dec 2020"
func formatedDate(_ dateString: String) -> String {
    
    let dateFormatterGet = DateFormatter()
    dateFormatterGet.dateFormat = "yyyy-MM-dd"

    let dateFormatterPrint = DateFormatter()
    dateFormatterPrint.dateFormat = "dd MMM yyyy"

    if let date = dateFormatterGet.date(from: dateString) {
        return dateFormatterPrint.string(from: date)
    } else {
       return ""
    }
}

func getDiffTimeString(_ dateString: String) -> String {
    
    let timeformatter = DateFormatter()
    timeformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    
    guard let time1 = timeformatter.date(from: dateString) else { return "" }

    let interval = Date().timeIntervalSince(time1)
    let day = interval / 3600 / 24
    let hour = interval / 3600
    let minute = interval.truncatingRemainder(dividingBy: 3600) / 60
    //let intervalInt = Int(interval)
    //print("diff time===", intervalInt)
    //return "\(intervalInt < 0 ? "-" : "+") \(Int(day)) Days \(Int(hour)) Hours \(Int(minute)) Minutes"
    if Int(day) > 0 {
        return "\(Int(day)) days"
    } else if Int(hour) > 0 {
        return "\(Int(hour)) hours"
    } else {
        return "\(Int(minute)) Min"
    }
}


//MARK:- saveToFile
// save image to a file (Documents/LLSeller/temp.png)
//MARK:-
func saveToFile(image: UIImage!, filePath: String!, fileName: String) -> String! {
    
    let outputFileName = fileName
    
    let outputImage = resizeImage(srcImage: image)
    
    let fileManager = FileManager.default
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
    var documentDirectory: NSString! = paths[0] as NSString?
    
    // current document directory
    fileManager.changeCurrentDirectoryPath(documentDirectory as String)
    
    do {
        try fileManager.createDirectory(atPath: filePath, withIntermediateDirectories: true, attributes: nil)
    } catch let error as NSError {
        print(error.localizedDescription)
    }
    
    documentDirectory = documentDirectory.appendingPathComponent(filePath) as NSString?
    let savedFilePath = documentDirectory.appendingPathComponent(outputFileName)
    
    // if the file exists already, delete and write, else if create filePath
    if (fileManager.fileExists(atPath: savedFilePath)) {
        do {
            try fileManager.removeItem(atPath: savedFilePath)
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
    } else {
        fileManager.createFile(atPath: savedFilePath, contents: nil, attributes: nil)
    }
    
    if let data = outputImage.pngData() {
        
        do {
            try data.write(to:URL(fileURLWithPath:savedFilePath), options:.atomic)
        } catch {
            print(error)
        }
    }
    return savedFilePath
}

func resizeImage(srcImage: UIImage) -> UIImage {

    if (srcImage.size.width >= srcImage.size.height) {

        return srcImage.resize(toTargetSize: CGSize(width: 256, height: 330))
    } else {

        return srcImage.resize(toTargetSize: CGSize(width: 256, height: 330))
    }
}

//MARK:- Create Thumbnail Image
//MARK:-
// Get Thumbnail Image from URL
fileprivate func getThumbnailFromUrl(_ url: String?, _ completion: @escaping ((_ image: UIImage?)->Void)) {

    guard let url = URL(string: url ?? "") else { return }
    DispatchQueue.main.async {
        let asset = AVAsset(url: url)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true

        let time = CMTimeMake(value: 2, timescale: 1)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            completion(thumbnail)
        } catch {
            print("Error :: ", error.localizedDescription)
            completion(nil)
        }
    }
}

func createThumbnailOfVideoFromRemoteUrl(url: String) -> UIImage? {
    let asset = AVAsset(url: URL(string: url)!)
    let assetImgGenerate = AVAssetImageGenerator(asset: asset)
    assetImgGenerate.appliesPreferredTrackTransform = true
    //Can set this to improve performance if target size is known before hand
    //assetImgGenerate.maximumSize = CGSize(width,height)
    let time = CMTimeMakeWithSeconds(1.0, preferredTimescale: 600)
    do {
        let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
        let thumbnail = UIImage(cgImage: img)
        return thumbnail
    } catch {
      print(error.localizedDescription)
      return nil
    }
}

func createThumbnailImage(videopath: URL) -> UIImage? {
    let asset = AVURLAsset(url: videopath)
    let generator = AVAssetImageGenerator(asset: asset)
    generator.appliesPreferredTrackTransform = true
    let timestamp = CMTime(seconds: 2, preferredTimescale: 60)
    if let imageRef = try? generator.copyCGImage(at: timestamp, actualTime: nil) {
        return UIImage(cgImage: imageRef)
    } else {
        return nil
    }
}

func getThumbnailFrom(path: URL) -> UIImage? {

    do {

        let asset = AVURLAsset(url: path , options: nil)
        let imgGenerator = AVAssetImageGenerator(asset: asset)
        imgGenerator.appliesPreferredTrackTransform = true
        let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
        let thumbnail = UIImage(cgImage: cgImage)

        return thumbnail

    } catch let error {

        print("*** Error generating thumbnail: \(error.localizedDescription)")
        return nil

    }

}





