//
//  ApiRequest.swift
//  Pixil
//
//  Created by Developer on 8/19/19.
//  Copyright © 2019 Ankhbayar Batbaatar. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ApiRequest {
    
    // MARK: - Request URLS
    static let BASE_URL                             = "http://scavengeski.com/"
    static let SERVER_URL                           = BASE_URL + "api/"
    
    static let REQ_REGISTER                         = SERVER_URL + "user/register"
    static let REQ_LOGIN                            = SERVER_URL + "user/login"
    static let REQ_SOCIAL_LOGIN                     = SERVER_URL + "user/social_login"
    static let REQ_FORGOT                           = SERVER_URL + "user/forgot"
    static let REQ_RESET_PWD                        = SERVER_URL + "user/reset_password"
    static let REQ_EDIT_USER                        = SERVER_URL + "user/edit_user"
    static let REQ_UPLOAD_PHOTO                     = SERVER_URL + "user/upload_photo"
    static let REQ_REGISTER_TOKEN                   = SERVER_URL + "user/register_token"
    
    static let REQ_GET_GAME                         = SERVER_URL + "post/get_game"
    static let REQ_SEARCH_GAME                      = SERVER_URL + "post/search_game"
    static let REQ_GET_TASK                         = SERVER_URL + "post/get_task"
    static let REQ_GET_ALL_POST                     = SERVER_URL + "post/get_all_post"
    static let REQ_JOIN_GAME                        = SERVER_URL + "post/join_game"
    static let REQ_ADD_POST                         = SERVER_URL + "post/add_post"
    static let REQ_LEAVE_GAME                       = SERVER_URL + "post/leave_game"
    static let REQ_GET_CATEGORY                     = SERVER_URL + "post/get_category"
    static let REQ_GET_TEAM                         = SERVER_URL + "post/get_team"
    
    // MARK: - APIs
    
    static func register(_ user: UserModel, completion: @escaping (String) -> ()) {
        
        let params: [String: String] = ["name": user.name,
                                        "email": user.email,
                                        "password": user.password,
                                        "phone": user.phone_no]
        
        Alamofire.upload( multipartFormData: { (formData) in
            
            for (key, value) in params {
                if let data = value.data(using: .utf8) {
                    formData.append(data, withName: key)
                }
            }
            
            formData.append(URL(fileURLWithPath: user.photo_url), withName: "photo")
           
            
        }, to: REQ_REGISTER, method: .post) { (result) in
            
            switch result {
                
            case .success(let upload, _, _):
                    
                upload.responseJSON { response in
                        
                    print("-----register response-----", response)
                        
                    switch response.result {
                        
                    case .success(_):
                    
                        let json = JSON(response.result.value!)
                        let resCode = json[Const.RESULT_CODE].intValue
                        if resCode == Const.CODE_SUCCESS {
                            user.user_id = json["id"].intValue
                            user.photo_url = json["photo_url"].stringValue
                            currentUser = user
                            completion(Const.MSG_SUCCESS)
                        } else {
                            completion(json[Const.RES_MESSAGE].stringValue)
                        }
                    case .failure(_):
                    
                        completion(Const.ERROR_CONNECT)
                    }
                }
            case .failure(_):
            
                completion(Const.ERROR_CONNECT)
            }
        }
    }
    
    static func social_login(_ user: UserModel, completion: @escaping (Int, String) -> ()) {
        
        let params = ["name": user.name,
                      "email": user.email,
                      "social_id": user.password,
                      "login_type": user.login_type,
                      "photo_url": user.photo_url] as [String : Any]
        
        Alamofire.request(REQ_SOCIAL_LOGIN, method: .post, parameters: params).responseJSON { response in
            
            print("--------  register social response --------\n", response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if resCode == Const.CODE_SUCCESS {
                    user.user_id = json["user_data"]["id"].intValue
                    currentUser = user
                    completion(resCode, Const.MSG_SUCCESS)
                } else if resCode == Const.CODE_203 {
                    user.user_id = json["id"].intValue
                    currentUser = user
                    completion(resCode, Const.MSG_SUCCESS)
                } else {
                    completion(Const.CODE_FAIL, json[Const.RES_MESSAGE].stringValue)
                }
            } else {
                completion(Const.CODE_FAIL, Const.ERROR_CONNECT)
            }
        }
    }
    
    static func login(email: String, password: String, completion: @escaping (String) -> () ) {
        
        let params = ["email" : email, "password" : password] as [String: Any]
        
        Alamofire.request(REQ_LOGIN, method: .post, parameters: params).responseJSON { response in
            
            print("--------------- login response -----------------\n", response)
            
            if response.result.isFailure {
                completion(Const.ERROR_CONNECT)
            }
            else
            {
                let json = JSON(response.result.value!)
                
                let resCode = json[Const.RESULT_CODE].intValue
                
                if resCode == Const.CODE_SUCCESS {
                    let userObject = json["user_data"]
                    let user = ParseHelper.parseUser(userObject)
                    user.password = password
                    currentUser = user
                    completion(Const.MSG_SUCCESS)
                    
                } else {
                    completion(json[Const.RES_MESSAGE].stringValue)
                }
            }
        }
    }
    
    static func forgot(email: String, completion: @escaping (String, String?) -> () ) {
        
        let params = ["email" : email] as [String: Any]
        
        Alamofire.request(REQ_FORGOT, method: .post, parameters: params).responseJSON { response in
            
            print("----- forgot response ------\n", response)
            
            if response.result.isFailure {
                completion(Const.ERROR_CONNECT, nil)
            }
            else
            {
                let json = JSON(response.result.value!)
                
                let resCode = json[Const.RESULT_CODE].intValue
                
                if resCode == Const.CODE_SUCCESS {
                    let pincode = json["pincode"].stringValue
                    completion(Const.MSG_SUCCESS, pincode)
                    
                } else {
                    completion(json[Const.RES_MESSAGE].stringValue, nil)
                }
            }
        }
    }
    
    static func reset_pwd(email: String, password: String, completion: @escaping (String) -> () ) {
        
        let params = ["email" : email, "password" : password] as [String: Any]
        
        Alamofire.request(REQ_RESET_PWD, method: .post, parameters: params).responseJSON { response in
            
            print("----- reset pwd response -----\n", response)
            
            if response.result.isFailure {
                completion(Const.ERROR_CONNECT)
            }
            else
            {
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if resCode == Const.CODE_SUCCESS {
                    completion(Const.MSG_SUCCESS)
                } else {
                    completion(json[Const.RES_MESSAGE].stringValue)
                }
            }
        }
    }
    
    static func update_photo(_ imgUrl: String, completion: @escaping (String) -> ()) {
        
        Alamofire.upload( multipartFormData: { (formData) in
            formData.append("\(currentUser!.user_id)".data(using: .utf8)!, withName: "user_id")
            formData.append(URL(fileURLWithPath: imgUrl), withName: "photo")
            
        }, to: REQ_UPLOAD_PHOTO, method: .post) { (result) in
            
            switch result {
                
            case .success(let upload, _, _):
                    
                upload.responseJSON { response in
                        
                    print("-----update photo response-----", response)
                        
                    switch response.result {
                        
                    case .success(_):
                    
                        let json = JSON(response.result.value!)
                        let resCode = json[Const.RESULT_CODE].intValue
                        if (resCode == Const.CODE_SUCCESS) {
                            currentUser?.photo_url = json["photo_url"].stringValue
                            completion(Const.MSG_SUCCESS)
                        } else {
                            completion(json[Const.RES_MESSAGE].stringValue)
                        }
                        
                    case .failure(_):
                    
                        completion(Const.ERROR_CONNECT)
                    }
                }
            case .failure(_):
            
                completion(Const.ERROR_CONNECT)
            }
        }
    }
    
    static func edit_user(_ user: UserModel, completion: @escaping (String) -> ()) {
        
        let params: [String: String] = [
            "user_id": "\(user.user_id)",
            "name": user.name,
            "email": user.email,
            "password": user.password,
            "phone": user.phone_no]
        
        Alamofire.request(REQ_EDIT_USER, method: .post, parameters: params).responseJSON { response in
            
            print("------  edit user response -----\n", response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if resCode == Const.CODE_SUCCESS {
                    currentUser = user
                    completion(Const.MSG_SUCCESS)
                } else {
                    completion(json[Const.RES_MESSAGE].stringValue)
                }
            } else {
                completion(Const.ERROR_CONNECT)
            }
        }
    }
    
    static func get_game(completion: @escaping ([GameModel], String) -> () ) {
        
        let param: [String: String] = ["user_id": "\(currentUser!.user_id)"]
        
        Alamofire.request(REQ_GET_GAME, method: .post, parameters: param).responseJSON { response in
            
            print("---- get game response -----\n", response)
            
            if response.result.isFailure {
                completion([], Const.ERROR_CONNECT)
            } else {
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                var objList = [GameModel]()
                if resCode == Const.CODE_SUCCESS {
                    let list = json["game_list"].arrayValue
                    for objc in list {
                        let one = ParseHelper.parseGame(objc)
                        objList.append(one)
                    }
                    completion(objList, Const.MSG_SUCCESS)
                }else {
                    completion([], json[Const.RES_MESSAGE].stringValue)
                }
            }
        }
    }
    
    static func search_game(_ keyword: String, completion: @escaping ([GameModel], String) -> () ) {
        
        let param: [String: String] = ["user_id": "\(currentUser!.user_id)", "name": keyword]
        
        Alamofire.request(REQ_SEARCH_GAME, method: .post, parameters: param).responseJSON { response in
            
            print("---- search game response -----\n", response)
            
            if response.result.isFailure {
                completion([], Const.ERROR_CONNECT)
            } else {
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                var objList = [GameModel]()
                if resCode == Const.CODE_SUCCESS {
                    let list = json["game_list"].arrayValue
                    for objc in list {
                        let one = ParseHelper.parseGame(objc)
                        objList.append(one)
                    }
                    completion(objList, Const.MSG_SUCCESS)
                } else {
                    completion([], json[Const.RES_MESSAGE].stringValue)
                }
            }
        }
    }
    
    static func join_game(_ game_id: Int, completion: @escaping (String) -> () ) {
        
        let param: [String: String] = ["user_id": "\(currentUser!.user_id)",
                                       "game_id": "\(game_id)"]
        
        Alamofire.request(REQ_JOIN_GAME, method: .post, parameters: param).responseJSON { response in
            
            print("---- join game response -----\n", response)
            
            if response.result.isFailure {
                completion(Const.ERROR_CONNECT)
            } else {
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                
                if resCode == Const.CODE_SUCCESS {
                    completion(Const.MSG_SUCCESS)
                } else {
                    completion(Const.MSG_SOMETHING_WRONG)
                }
            }
        }
    }
    
    static func get_task(_ game_id: Int, completion: @escaping ([TaskModel], String) -> () ) {
        
        let param: [String: String] = ["game_id": "\(game_id)"]
        
        Alamofire.request(REQ_GET_TASK, method: .post, parameters: param).responseJSON { response in
            
            print("---- get task response -----\n", response)
            
            if response.result.isFailure {
                completion([], Const.ERROR_CONNECT)
            } else {
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                var objList = [TaskModel]()
                if resCode == Const.CODE_SUCCESS {
                    let list = json["task_list"].arrayValue
                    for objc in list {
                        let one = ParseHelper.parseTask(objc)
                        objList.append(one)
                    }
                    completion(objList, Const.MSG_SUCCESS)
                }
            }
        }
    }
    
    static func get_all_post(_ game_id: Int, completion: @escaping ([PostModel], String) -> () ) {
        
        let param: [String: Any] = ["game_id": SelectedGame!.id]
        
        Alamofire.request(REQ_GET_ALL_POST, method: .post, parameters: param).responseJSON { response in
            
            print("---- get post response -----\n", response)
            
            if response.result.isFailure {
                completion([], Const.ERROR_CONNECT)
            } else {
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                var objList = [PostModel]()
                if resCode == Const.CODE_SUCCESS {
                    let list = json["post_list"].arrayValue
                    for objc in list {
                        let one = ParseHelper.parsePost(objc)
                        objList.append(one)
                    }
                    completion(objList, Const.MSG_SUCCESS)
                }
            }
        }
    }
    
    static func get_category( completion: @escaping ([CategoryModel], String) -> () ) {
        
        Alamofire.request(REQ_GET_CATEGORY, method: .post).responseJSON { response in
            
            print("---- get category response -----\n", response)
            
            if response.result.isFailure {
                completion([], Const.ERROR_CONNECT)
            } else {
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                var objList = [CategoryModel]()
                if resCode == Const.CODE_SUCCESS {
                    let list = json["category_list"].arrayValue
                    for objc in list {
                        let one = ParseHelper.parseCategory(objc)
                        objList.append(one)
                    }
                    completion(objList, Const.MSG_SUCCESS)
                }
            }
        }
    }
    
    static func add_post(_ task_id: Int, comment: String, photoUrl: String, videoUrl: String, completion: @escaping (String) -> ()) {
        
        let params: [String: String] = [
            "game_id": "\(SelectedGame!.id)",
            "user_id": "\(currentUser!.user_id)",
            "task_id": "\(task_id)",
            "comment": comment
        ]
        
        Alamofire.upload( multipartFormData: { (formData) in
            for (key, value) in params {
                if let data = value.data(using: .utf8) {
                    formData.append(data, withName: key)
                }
            }
            if photoUrl != "" {
                formData.append(URL(fileURLWithPath: photoUrl), withName: "post[\(0)]")
            }
            if videoUrl != "" {
                formData.append(URL(fileURLWithPath: videoUrl), withName: "post[\(1)]")
            }
            
        }, to: REQ_ADD_POST, method: .post) { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    print("-----add post response-----", response)
                    switch response.result {
                    case .success(_):
                    
                        let json = JSON(response.result.value!)
                        let resCode = json[Const.RESULT_CODE].intValue
                        if (resCode == Const.CODE_SUCCESS) {
                            completion(Const.MSG_SUCCESS)
                        } else {
                            completion(json[Const.RES_MESSAGE].stringValue)
                        }
                    case .failure(_):
                        completion(Const.ERROR_CONNECT)
                    }
                }
            case .failure(_):
                completion(Const.ERROR_CONNECT)
            }
        }
    }
    
    static func register_token(_ token: String, completion: @escaping (String) -> () ) {
        
        let param : [String: String] = ["user_id": "\(currentUser!.user_id)", "token": token]
        print("currentuser token===\(currentUser!.token)")
        Alamofire.request(REQ_REGISTER_TOKEN, method: .post, parameters: param).responseJSON { response in
            
            print("--------------- register token response -----------------\n", response)
            
            if response.result.isFailure {
                completion(Const.ERROR_CONNECT)
            } else {
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if resCode == Const.CODE_SUCCESS {
                    completion(Const.MSG_SUCCESS)
                }
            }
        }
    }
    
    static func leave_game(_ game_id: Int, completion: @escaping (String) -> () ) {
        
        let param: [String: String] = ["user_id": "\(currentUser!.user_id)",
                                       "game_id": "\(game_id)"]
        
        Alamofire.request(REQ_LEAVE_GAME, method: .post, parameters: param).responseJSON { response in
            
            print("---- leave game response -----\n", response)
            
            if response.result.isFailure {
                completion(Const.ERROR_CONNECT)
            } else {
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                
                if resCode == Const.CODE_SUCCESS {
                    completion(Const.MSG_SUCCESS)
                } else {
                    completion(Const.MSG_SOMETHING_WRONG)
                }
            }
        }
    }
    
    static func get_team(_ game_id: Int, completion: @escaping ([TeamModel], [IndividualModel], String) -> () ) {
    
        let param: [String: String] = ["game_id": "\(game_id)"]
        
        Alamofire.request(REQ_GET_TEAM, method: .post, parameters: param).responseJSON { response in
            
            print("---- get team response -----\n", response)
            
            if response.result.isFailure {
                completion([], [], Const.ERROR_CONNECT)
            } else {
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                var objList = [TeamModel]()
                var individualList = [IndividualModel]()
                if resCode == Const.CODE_SUCCESS {
                    let list = json["team_list"].arrayValue
                    for objc in list {
                        let one = ParseHelper.parseTeam(objc)
                        objList.append(one)
                        individualList += one.members
                    }
                    completion(objList, individualList, Const.MSG_SUCCESS)
                }
            }
        }
    }
}
