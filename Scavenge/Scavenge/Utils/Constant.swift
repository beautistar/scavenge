//
//  Constant.swift
//  Ride
//
//  Created by Yin on 2018/9/6.
//  Copyright © 2018 Yin. All rights reserved.
//

import Foundation
import UIKit


class Const {

    static let APPNAME                      = "Scavenge"
    static let OK                           = "OK"
    static let CANCEL                       = "Cancel"
    static let NO                           = "No"
    static let YES                          = "Yes"
    
    static let SUPPORT_MAIL                 = "scavengehelpandfeedback@gmail.com"
    
    static let COLOR_MAIN                   = UIColor(named: "MainBlueColor")
    static let COLOR_BG                     = UIColor(named: "LightBlueBgColor")
    static let COLOR_GREEN                  = UIColor(named: "GreenColor")
    //error messages
    static let CHECK_NAME_EMPTY             = "Please enter name"
    static let CHECK_EMAIL_EMPTY            = "Please enter your email address"
    static let CHECK_VAILD_EMAIL            = "Please enter valid email"
    static let CHECK_EMPTY_PHONE            = "Please enter your phone number"
    static let CHECK_PASSWORD               = "Please enter password"
    

    static let MSG_SUCCESS                  = "Success"
    static let MSG_FAIL                     = "Fail"
    static let ERROR_CONNECT                = "Failed to server connection"
    static let MSG_SOMETHING_WRONG          = "Something went wrong"
    
    // Request Params
    
    // Response parameters
    static let RES_MESSAGE                  = "message"

    
    // Key
    static let KEY_USERID                   = "k_userid"
    static let KEY_NAME                     = "k_name"
    static let KEY_TOKEN                    = "k_token"
    static let KEY_EMAIL                    = "k_email"
    static let KEY_PASSWORD                 = "k_password"
    static let KEY_TYPE                     = "k_type"
    
    // Result code
    static let RESULT_CODE                  = "result_code"
    static let CODE_201                     = 201
    static let CODE_SUCCESS                 = 200
    static let CODE_202                     = 202
    static let CODE_203                     = 203
    static let CODE_FAIL                    = 400    
}
