//
//  ParseHelper.swift
//  Transform
//
//  Created by Yin on 26/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//


import Foundation
import SwiftyJSON

class ParseHelper {
    
    static func parseUser(_ userObject: JSON) -> UserModel {
        
        let user = UserModel()
        
        user.user_id = userObject["id"].intValue
        user.name = userObject["name"].stringValue
        user.email = userObject["email"].stringValue
        user.phone_no = userObject["phone"].stringValue
        user.photo_url = userObject["photo_url"].stringValue
        user.login_type = userObject["login_type"].stringValue        
        
        return user
    }
    
    static func parseGame(_ object: JSON) -> GameModel {
        
        let game = GameModel()
        
        game.id = object["id"].intValue
        game.name = object["name"].stringValue
        game.comment_count = object["comment_count"].intValue
        game.photo_url = object["photo_url"].stringValue
        game.start_date = object["start_date"].stringValue
        game.end_date = object["end_date"].stringValue
        game.is_joined = object["is_joined"].stringValue
        game.description = object["description"].stringValue
        
        return game
    }
    
    static func parseTask(_ object: JSON) -> TaskModel {
        
        let model = TaskModel()
        
        model.id = object["id"].intValue
        model.name = object["name"].stringValue
        model.category = object["category"].stringValue
        model.photo_url = object["photo_url"].stringValue
        model.description = object["description"].stringValue
        model.point = object["point"].intValue
        model.post_count = object["post_count"].intValue
        model.is_completed = object["is_completed"].stringValue
        return model
    }
    
    static func parsePost(_ object: JSON) -> PostModel {
        
        let model = PostModel()
        
        model.id = object["id"].intValue
        model.photo_url = object["photo_url"].stringValue
        model.video_url = object["video_url"].stringValue
        model.comment = object["comment"].stringValue
        model.created_at = object["created_at"].stringValue
        model.owner.user_id = object["user_id"].intValue
        model.owner.name = object["user_name"].stringValue
        model.owner.photo_url = object["user_photo_url"].stringValue
        model.point = object["point"].stringValue
        model.name = object["name"].stringValue
        
        return model
    }
    
    static func parseCategory(_ object: JSON) -> CategoryModel {
        
        let model = CategoryModel()
        
        model.name = object["name"].stringValue
        
        return model
    }
    
    static func parseTeam(_ object: JSON) -> TeamModel {
        
        let model = TeamModel()
        
        model.teamId = object["id"].intValue
        model.teamName = object["name"].stringValue
        
        let membersList = object["members"].arrayValue
        var teamTotalScore = 0
        var teamAchievedScore = 0
        for memberObject in membersList {
            let member = IndividualModel()
            member.name = memberObject["name"].stringValue
            member.userPhoto = memberObject["photo_url"].stringValue
            member.score = memberObject["achieved_point"].intValue
            member.totalScore = memberObject["available_point"].intValue
            member.teamName = model.teamName
            model.members.append(member)
            teamTotalScore += member.totalScore
            teamAchievedScore += member.score
        }
        
        model.teamTotalScore = teamTotalScore
        model.teamScore = teamAchievedScore
        
        return model
    }
}
