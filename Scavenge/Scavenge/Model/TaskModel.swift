//
//  TaskModel.swift
//  Scavenge
//
//  Created by Developer on 2/29/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class TaskModel {
    
    var id: Int = 0
    var name = ""
    var category = ""
    var description = ""
    var point = 0
    var photo_url = ""
    var post_count = 0
    var is_completed = "no"

}
