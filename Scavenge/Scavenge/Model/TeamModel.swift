//
//  TeamModel.swift
//  Scavenge
//
//  Created by cs on 2020/3/27.
//  Copyright © 2020 Developer. All rights reserved.
//

import Foundation

class TeamModel {
    
    var teamId : Int = 0
    var teamName = ""
    var teamScore = 0
    var teamTotalScore = 0
    
    var members = [IndividualModel]()

}
