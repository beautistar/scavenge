//
//  UserModel.swift
//  Pixil
//
//  Created by Developer on 8/19/19.
//  Copyright © 2019 Ankhbayar Batbaatar. All rights reserved.
//

import Foundation

class UserModel {
    
    var user_id: Int = 0
    var name = ""
    var email = ""
    var phone_no = ""
    var password = ""
    var photo_url = ""
    var login_type = ""
    var token: String {
        get {
            if let token = UserDefaults.standard.value(forKey: Const.KEY_TOKEN) {
                return token as! String
            }
            return ""
        }
    }
}
