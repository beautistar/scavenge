//
//  GameModel.swift
//  Scavenge
//
//  Created by Developer on 2/11/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class GameModel {
    
    var id: Int = 0
    var name = ""
    var start_date = ""
    var end_date = ""
    var comment_count = 0
    var photo_url = ""
    var is_joined = ""
    var description = ""
}
