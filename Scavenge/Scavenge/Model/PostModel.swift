//
//  PostModel.swift
//  Scavenge
//
//  Created by Developer on 2/29/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class PostModel {
    
    var id = 0
    var photo_url = ""
    var video_url = ""
    var comment = ""
    var created_at = ""
    var point = ""
    var name = ""
    
    var owner = UserModel()

}
