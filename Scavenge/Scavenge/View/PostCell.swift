//
//  PostCell.swift
//  Scavenge
//
//  Created by Developer on 2/13/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class PostCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var btnPlayVideo: UIButton!
    @IBOutlet weak var imvUserPhoto: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblScore: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    var entity: PostModel! {
        didSet {
            imvPhoto.kf.indicatorType = .activity
            imvPhoto.kf.setImage(with: URL(string: entity.photo_url), placeholder: UIImage(named: "profile_logo"))
            imvUserPhoto.kf.indicatorType = .activity
            imvUserPhoto.kf.setImage(with: URL(string: entity.owner.photo_url), placeholder: UIImage(named: "user_placeholder"))
            lblUserName.text = entity.owner.name
            lblTime.text = "  " + getDiffTimeString(entity.created_at) + " ago  "
            lblComment.text = entity.comment
            lblTitle.text = entity.name
            lblScore.text = entity.point
            if entity.video_url == "" {
                btnPlayVideo.isHidden = true
            } else {
                btnPlayVideo.isHidden = false
            }
        }
    }
}
