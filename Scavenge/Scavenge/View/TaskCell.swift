//
//  TaskCell.swift
//  Scavenge
//
//  Created by Developer on 2/12/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class TaskCell: UITableViewCell {

    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPoint: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblChanllenge: UILabel!
    @IBOutlet weak var btnPlus: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var entity: TaskModel! {
        didSet {
            imvPhoto.kf.indicatorType = .activity
            imvPhoto.kf.setImage(with: URL(string: entity.photo_url), placeholder: UIImage(named: "placeholder"))
            lblName.text = entity.name
            lblDescription.text = entity.description
            lblChanllenge.text = "#\(entity.category)"
            lblPoint.text = "\(entity.point) Pt"
        }
    }

}
