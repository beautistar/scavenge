//
//  GameCell.swift
//  Scavenge
//
//  Created by Developer on 2/10/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class GameCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var btnJoin: UIButton!
    @IBOutlet weak var imvOwnerProfile: UIImageView!
    @IBOutlet weak var imvOwnerName: UILabel!
    @IBOutlet weak var lblCommentCount: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var entity: GameModel! {
        
        didSet {
            imvPhoto.kf.indicatorType = .activity
            imvPhoto.kf.setImage(
                with: URL(string: entity.photo_url),
            placeholder: UIImage(named: "placeholder"))
            lblName.text = entity.name
            lblStartDate.text = formatedDate(entity.start_date)
            lblEndDate.text = formatedDate(entity.end_date)
            lblCommentCount.text = "\(entity.comment_count) Comments"
        }
    }
}
