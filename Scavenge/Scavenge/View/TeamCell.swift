//
//  TeamCell.swift
//  Scavenge
//
//  Created by Developer on 2/20/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit
import LinearProgressBar
import ExpyTableView
class TeamCell: UITableViewCell {

    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var barProgress: LinearProgressBar!
    @IBOutlet weak var lblCurrentValue: UILabel!
    @IBOutlet weak var lblCurrentValueXConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTotalValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var entity: TeamModel! {
        didSet {
            lblTeamName.text = entity.teamName
            lblCurrentValue.text = "\(entity.teamScore)"
            lblTotalValue.text = "\(entity.teamTotalScore)"
            barProgress.progressValue = CGFloat(entity.teamTotalScore == 0 ? 0 : Float(entity.teamScore)/Float(entity.teamTotalScore))*100
            lblCurrentValueXConstraint.constant = barProgress.bounds.origin.x + barProgress.bounds.size.width * CGFloat(entity.teamTotalScore == 0 ? 0 : Float(entity.teamScore)/Float(entity.teamTotalScore)) - lblCurrentValue.bounds.size.width
        }
    }
}
