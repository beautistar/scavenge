//
//  CategoryCell.swift
//  Scavenge
//
//  Created by Developer on 2/12/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {
    
    @IBOutlet weak var lblName: UILabel!    
    
    var entity : CategoryModel! {
        
        didSet{
            
            lblName.text = entity.name
            
            if entity.selectState {
                lblName.backgroundColor = UIColor(named: "GreenColor")
                lblName.textColor = UIColor.white
                lblName.layer.borderWidth = 0
            } else {
                lblName.backgroundColor = .white
                lblName.textColor = UIColor.black
                lblName.layer.borderWidth = 1
                lblName.layer.borderColor = UIColor.black.cgColor
            }
        }
    }
}
