//
//  TaskCollectionViewCell.swift
//  Scavenge
//
//  Created by Developer on 2/14/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class TaskCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var lblPoint: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    var entity: TaskModel! {
        didSet {
            imvPhoto.kf.indicatorType = .activity
            imvPhoto.kf.setImage(with: URL(string: entity.photo_url), placeholder: UIImage(named: "placeholder"))
            lblName.text = entity.name
            lblDescription.text = entity.description
            lblPoint.text = "\(entity.point) Pt"
        }
    }    
}
