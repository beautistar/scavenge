//
//  IndividualCell.swift
//  Scavenge
//
//  Created by Developer on 2/20/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit
import LinearProgressBar

class IndividualCell: UITableViewCell {
    
    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var barProgress: LinearProgressBar!
    @IBOutlet weak var lblCurrentValue: UILabel!
    @IBOutlet weak var lblCurrentValueXConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTotalValue: UILabel!
    @IBOutlet weak var imvPhotoXConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTeamNameXConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var entity: IndividualModel! {
        didSet {
            imvPhoto.kf.indicatorType = .activity
            imvPhoto.kf.setImage(with: URL(string: entity.userPhoto), placeholder: UIImage(named: "placeholder"))
            lblUserName.text = entity.name
            lblTeamName.text = entity.teamName
    
            barProgress.progressValue = CGFloat(entity.totalScore == 0 ? 0 : Float(entity.score)/Float(entity.totalScore))*100
            
            lblCurrentValueXConstraint.constant = barProgress.bounds.origin.x + barProgress.bounds.size.width * CGFloat(entity.totalScore == 0 ? 0 : Float(entity.score)/Float(entity.totalScore)) - lblCurrentValue.bounds.size.width
            lblCurrentValue.text = "\(entity.score)"
            lblTotalValue.text = "\(entity.totalScore)"
        }
    }
}
